# About
Chord is a program analysis platform that enables users to productively design, implement, combine, and evaluate 
a broad variety of static and dynamic program analyses for Java bytecode.

Chord is portable and has been tested on a variety of platforms, including Linux, Windows/Cygwin, and MacOS.
It is open source software distributed under the [New BSD License](http://www.opensource.org/licenses/bsd-license.php).

# Documentation

* User Guide in [HTML](http://www.seas.upenn.edu/~mhnaik/chord/user_guide/index.html) or [PDF](http://www.seas.upenn.edu/~mhnaik/chord/user_guide.pdf).
* [Tutorial](http://www.seas.upenn.edu/~mhnaik/chord/pldi11/tutorial.pptx) (presented at PLDI 2011).

# Talks

* [Chord: An Extensible Program Analysis Framework Using CnC](http://www.cis.upenn.edu/~mhnaik/slides/chord_cnc2010.ppt), 
  2nd Concurrent Collections Workshop, Oct 2010.
* [Mechanizing Program Analysis With Chord](http://www.cis.upenn.edu/~mhnaik/slides/chord_lfx2010.ppt), 
  1st Workshop on LFX: Learning From eXperience, Jun 2010.

# Questions?

For questions about Chord, send email to chord-discuss{at}googlegroups.com or
[browse the archives](http://groups.google.com/group/chord-discuss).
Posting does not require membership but posts by non-members are moderated to avoid spamming group members.

